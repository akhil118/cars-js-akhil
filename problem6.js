
function BMWAudiCars(inventory) {

    if (Array.isArray(inventory) && inventory.length !==0 ) {

        let carsList =[]
        for (eachItem of inventory) {

            if (eachItem["car_make"] === "Audi" || eachItem["car_make"] === "BMW") {
                carsList.push(eachItem)
            }
        }
        let result = JSON.stringify(carsList)
        return result
    }
    else {
        return "please enter valid inventory"
    }

}

module.exports = BMWAudiCars