

function carDetails(inventory,id) {

    if (Array.isArray(inventory) && inventory.length !==0 && typeof(id) === "number") {
        let count = 0
        for (let eachitem of inventory) {
            if (eachitem["id"] === id) {
                count = count+1
                string = `Car ${id} is a ${eachitem.car_year} ${eachitem.car_make} ${eachitem.car_model}`
                return string

            } 
        }
        if (count === 0 ) {
            return "car is not in inventory"
        }
    }
    else {
        return "please enter valid inventory and id"
    }
}


module.exports = carDetails